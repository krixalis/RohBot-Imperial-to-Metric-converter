# RohBot Imperial to Metric Converter

Automatically finds and converts the most common imperial units to metric. Only works in [RohBot](https://rohbot.net).

Only tested in Chrome using [Tampermonkey](http://tampermonkey.net/).

### Currently supports the following conversions:
* Feet to meters
* Inches to centimeters
* Yards to meters
* Fahrenheit to Celsius
* Pounds to kilograms
* Ounces to grams
* Gallons to liters
* Miles per hour to kilometers per hour